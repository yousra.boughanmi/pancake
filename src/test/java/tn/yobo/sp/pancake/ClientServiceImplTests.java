package tn.yobo.sp.pancake;


import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.yobo.sp.pancake.model.Client;
import tn.yobo.sp.pancake.service.ClientService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceImplTests {

	@Autowired
	ClientService clientService ;
	
	@Test
	public void insertClientTest() {
		int numberClient = clientService.findAll().size();
		
		Calendar cal =Calendar.getInstance();
        cal.set(1992,05,01);
		Client c = new Client("Yosra","Boughanmi",cal.getTime(),true);
		Client newClient = clientService.create(c);
		Assert.assertEquals(numberClient+1, clientService.findAll().size());
		Assert.assertNotNull(newClient.getFirstName());
		
	}
}
