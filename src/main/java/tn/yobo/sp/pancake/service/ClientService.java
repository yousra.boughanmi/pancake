package tn.yobo.sp.pancake.service;

import java.util.List;

import tn.yobo.sp.pancake.model.Client;

public interface ClientService {

	
	Client create(Client c);
	Client update(Client c);
	void delete(Client c);
	void delete(Long id);	
	Client findById(Long id);	
	List<Client> findAll();
}
