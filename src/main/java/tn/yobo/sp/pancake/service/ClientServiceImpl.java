package tn.yobo.sp.pancake.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.yobo.sp.pancake.model.Client;
import tn.yobo.sp.pancake.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService{
	
	@Autowired
	ClientRepository clientRepository;

	@Override
	public Client create(Client c) {
		return clientRepository.save(c);
	}

	@Override
	public Client update(Client c) {
		return clientRepository.save(c);
	}

	@Override
	public void delete(Client c) {
		clientRepository.delete(c);
	}

	@Override
	public void delete(Long id) {
		clientRepository.deleteById(id);
	}

	@Override
	public Client findById(Long id) {
		Optional<Client> client = clientRepository.findById(id);
		return client.isPresent() ? client.get() : null;		 
	}

	@Override
	public List<Client> findAll() {
		return clientRepository.findAll();
	}

}
