package tn.yobo.sp.pancake.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.yobo.sp.pancake.model.Client;
import tn.yobo.sp.pancake.service.ClientService;

@RestController
@RequestMapping(value = "/clients")
public class ClientRestApi {

	@Autowired
	ClientService clientService;

	@PostMapping(value = "/")
	public Client create(@RequestBody Client c) {
		return clientService.create(c);
	}

	@PutMapping(value = "/")
	public Client update(@RequestBody Client c) {
		if (clientService.findById(c.getId()) != null) {
			return clientService.create(c);
		}return null;
	}

	@DeleteMapping(value = "/")
	public void delete(@RequestBody Client c) {
		clientService.delete(c);
	}
	
	@GetMapping(value = "/delete/{id}")
	public void delete(@PathVariable Long id) {
		clientService.delete(id);
	}

	@GetMapping(value = "/{id}")
	public Client findById(@PathVariable Long id) {
		return clientService.findById(id);
	}
	
	@GetMapping(value = "/")
	public List<Client> findAll() {
		return clientService.findAll();
	}
}
