package tn.yobo.sp.pancake.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Client {

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String firstName;
	private String lastName;
	private Date birthDate;
	private Boolean loyal;
	
	public Client(String firstName, String lastName, Date birthDate, Boolean loyal) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.loyal = loyal;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Boolean getLoyal() {
		return loyal;
	}
	public void setLoyal(Boolean loyal) {
		this.loyal = loyal;
	}
	
	public float getAge() {
		return ((new Date().getTime() - this.birthDate.getTime()) / 1000 / 60 / 60 / 24 / 365    );

	}
	
	
	
}
